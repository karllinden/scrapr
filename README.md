# scrapr


## Introduction

Scrapr is a small command line interface for web scraping.

## Building and running
The following software is required to build scrapr:
 - OpenJDK 17
 - Maven. Scrapr have been tested to build with version 3.9.1, but other versions may work just as well.

Use this command to build scrapr and execute the test suite:
```bash
mvn clean verify
```

The build produces a distributable zip-file at `target/scrapr-${version}.zip`.
The following programs are needed to run scrapr:
 - A Java runtime environment (JRE), version 17 or later.
 - Bash, if you want to use the start script.

For example, to run version 1.0-SNAPSHOT of this project, run:
```bash
cd target
unzip scrapr-1.0-SNAPSHOT.zip
cd scrapr-1.0-SNAPSHOT
./scrapr
```

## Design

The program has been split into two packages: `org.scrapr` that contains the business logic
and `org.scrapr.cli` that contains the CLI specific classes.
Even though the separation is overkill for a program of this size, it is suitable to allow the
business logic to be reused in other contexts, such as behind a REST API or as part of a desktop
application, should that be necessary as the program grows.

The business logic has been made testable by using the strategy design pattern.
More precisely, `Fetcher` and `Saver` are strategies to the context class `Scraper`, which contains
all stateful business logic.
This makes it possible to test `Scraper` without making real HTTP requests and without writing real
files to the file system.

Methods that do not need to access any state of the `Scraper` have been implemented in
the `ScrapeUtils` class.
Currently, there is only one such method.

Regarding parallelization, the `Scraper` uses an `ExecutorService` that can be customized.
While this allows downloading to be parallelized, it also results in each thread being blocked by
download I/O, but it is not too bad at this scale.
In larger applications a fully non-blocking and asynchronous I/O solution would be needed to allow
the downloading threads to perform meaningful work for a larger part of the time.
For an application of this size, I think it is acceptable to dedicate a small number of threads for
downloading, and accept the small waste that it is having threads waiting.

The disk thread runs asynchronously with `Scraper` to avoid blocking the main thread from taking
care of completed downloads.
This is to ensure that the thread pool that takes care of the downloading always has work to do.
