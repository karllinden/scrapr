package org.scrapr;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.net.URI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link Scraper}.
 */
class ScraperTest {

  private FakeFetcher fetcher;
  private FakeSaver saver;

  private Scraper scraper;

  @BeforeEach
  void setUp() {
    fetcher = new FakeFetcher();
    saver = new FakeSaver();
    scraper = createScraper(URI.create("http://example.com"));
  }

  private Scraper createScraper(URI uri) {
    var scraper = new Scraper(uri);
    scraper.setFetcher(fetcher);
    scraper.setSaver(saver);
    return scraper;
  }

  @Test
  void shouldThrowIfUriIsNull() {
    assertThrows(NullPointerException.class, () -> new Scraper(null));
  }

  @Test
  void shouldThrowIfFetcherExecutorIsNull() {
    assertThrows(NullPointerException.class, () -> scraper.setFetcherExecutor(null));
  }

  @Test
  void shouldThrowIfUriDoesNotHaveScheme() {
    var uri = URI.create("books.toscrape.com/");
    var ex = assertThrows(IllegalArgumentException.class, () -> new Scraper(uri));
    assertThat(ex.getMessage(), containsString("The URI does not have a scheme (http or https)"));
  }

  @Test
  void shouldThrowIfUriDoesNotHaveAHost() {
    var uri = URI.create("http:/");
    assertThrows(IllegalArgumentException.class, () -> new Scraper(uri));
  }

  @Test
  void shouldThrowIfFetcherIsNull() {
    assertThrows(NullPointerException.class, () -> scraper.setFetcher(null));
  }

  @Test
  void shouldThrowIfSaverIsNull() {
    assertThrows(NullPointerException.class, () -> scraper.setSaver(null));
  }

  @Test
  void shouldScrapeRoot() throws IOException, InterruptedException {
    scraper.scrape();
    fetcher.assertScraped("/index.html");
  }

  @Test
  void shouldScrapeDesiredPathIfGiven() throws IOException, InterruptedException {
    var s = createScraper(URI.create("https://a.b/index.php"));
    fetcher.setExpectedScheme("https");
    fetcher.setExpectedHost("a.b");
    s.scrape();
    fetcher.assertScraped("/index.php");
  }

  @Test
  void shouldScrapeIndexHtmlIfRootIsGiven() throws IOException, InterruptedException {
    var s = createScraper(URI.create("http://example.com/"));
    s.scrape();
    fetcher.assertScraped("/index.html");
  }

  @Test
  void shouldSaveScrapedPage() throws IOException, InterruptedException {
    fetcher.putResource("/index.html", FakeResource.html("<html />"));
    scraper.scrape();
    saver.assertSaved("example.com/index.html", "<html />");
  }

  @Test
  void shouldFollowLinks() throws IOException, InterruptedException {
    String indexHtml = """
        <html>
          <body>
            Click <a href="/stuff.html">here</a>!
          </body>
        </html>
        """;
    String stuffHtml = """
        <html>
          <body>
            <h1>Welcome to scary stuff!</h1>
            <p>Want to <a href="/index.html">escape</a>?</p>
          </body>
        </html>
        """;
    fetcher.putResource("/index.html", FakeResource.html(indexHtml));
    fetcher.putResource("/stuff.html", FakeResource.html(stuffHtml));
    scraper.scrape();
    saver.assertSaved("example.com/index.html", indexHtml);
    saver.assertSaved("example.com/stuff.html", stuffHtml);
  }

  @Test
  void shouldNotExtractLinksFromImages() throws IOException, InterruptedException {
    String indexHtml = """
        <html>
          <body>
            <img src="/cats.svg" />
          </body>
        </html>
        """;
    String catsSvg = """
        <!-- What kind of image is this really? -->
        <a href="/duck.html">Duck</a>
        """;
    fetcher.putResource("/index.html", FakeResource.html(indexHtml));
    fetcher.putResource("/cats.svg", FakeResource.file(catsSvg).withContentType("image/svg+xml"));
    fetcher.putResource("/duck.html", FakeResource.html(catsSvg));
    scraper.scrape();
    saver.assertSaved("example.com/index.html", indexHtml);
    saver.assertSaved("example.com/cats.svg", catsSvg);
    saver.assertNotSaved("example.com/duck.html");
  }

  @Test
  void shouldNotFollowLinksWithDifferentSchemeOrHost() throws IOException, InterruptedException {
    fetcher.putResource("/index.html", FakeResource.html("""
        <html>
          <body>
            <img src="https://example.com/cats.png" />
            <img src="http://example.se/dogs.png" />
          </body>
        </html>
        """));
    scraper.scrape();
    saver.assertNotSaved("example.com/cats.png");
    saver.assertNotSaved("example.com/dogs.png");
  }

  @Test
  void shouldThrowIOExceptionIfSaverThrowsIOException() {
    fetcher.putResource("/index.html", FakeResource.html("<html />"));
    var exception = new IOException("Permission denied.");
    scraper.setSaver((path, bytes) -> {
      throw exception;
    });
    assertThrowsExactly(exception);
  }

  @Test
  void shouldThrowRuntimeExceptionIfSaverThrowsRuntimeException() {
    fetcher.putResource("/index.html", FakeResource.html("<html />"));
    var exception = new RuntimeException("Fake some bug...");
    scraper.setSaver((path, bytes) -> {
      throw exception;
    });
    assertThrowsExactly(exception);
  }

  @Test
  void shouldThrowIOExceptionIfFetcherThrowsIOException() {
    var exception = new IOException("DNS lookup failed");
    fetcher.putResource("/index.html", FakeResource.ioException(exception));
    assertThrowsExactly(exception);
  }

  @Test
  void shouldThrowInterruptedExceptionIfFetcherThrowsInterruptedException() {
    var exception = new InterruptedException();
    fetcher.putResource("/index.html", FakeResource.interruptedException(exception));
    assertThrowsExactly(exception);
  }

  @Test
  void shouldThrowRuntimeExceptionIfFetcherThrowsRuntimeException() {
    var exception = new RuntimeException();
    fetcher.putResource("/index.html", FakeResource.runtimeException(exception));
    assertThrowsExactly(exception);
  }

  // Asserts that the scraper throws exactly the given exception.
  private void assertThrowsExactly(Exception exception) {
    var ex = assertThrows(Exception.class, scraper::scrape);
    assertSame(exception, ex);
  }

}
