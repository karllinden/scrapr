package org.scrapr;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * Fake implementation of {@link Saver}.
 */
final class FakeSaver implements Saver {

  private final Map<Path, byte[]> files = new HashMap<>();

  @Override
  public void save(Path path, byte[] bytes) {
    assertNotNull(path);
    files.put(path, bytes.clone());
  }

  public void assertSaved(String path, String contents) {
    byte[] bytes = files.get(Path.of(path));
    assertNotNull(bytes);
    assertEquals(contents, new String(bytes, StandardCharsets.UTF_8));
  }

  public void assertNotSaved(String path) {
    assertNull(files.get(Path.of(path)));
  }

}
