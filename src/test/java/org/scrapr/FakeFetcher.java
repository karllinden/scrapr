package org.scrapr;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Fake implementation of {@link Fetcher}.
 */
final class FakeFetcher implements Fetcher {

  private final Map<String, FakeResource> resources = new HashMap<>();

  private final Set<String> scrapedPaths = new HashSet<>();

  private String expectedScheme = "http";

  private String expectedHost = "example.com";

  public void putResource(String path, FakeResource resource) {
    Objects.requireNonNull(path);
    Objects.requireNonNull(resource);
    resources.put(path, resource);
  }

  public void setExpectedScheme(String scheme) {
    this.expectedScheme = Objects.requireNonNull(scheme);
  }

  public void setExpectedHost(String host) {
    this.expectedHost = Objects.requireNonNull(host);
  }

  @Override
  public HttpResponse<byte[]> send(HttpRequest request) throws InterruptedException, IOException {
    URI uri = request.uri();

    assertEquals(expectedScheme, uri.getScheme());
    assertEquals(expectedHost, uri.getHost());
    String path = uri.getPath();
    assertNotNull(path);
    scrapedPaths.add(path);

    FakeResource resource = resources.getOrDefault(path, FakeResource.absent());
    return resource.toHttpResponse(request);
  }

  public void assertScraped(String path) {
    assertThat(scrapedPaths, hasItem(path));
  }

}
