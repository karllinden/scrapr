package org.scrapr;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.scrapr.ScrapeUtils.extractLinks;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link ScrapeUtils}.
 */
class ScrapeUtilsTest {

  @Test
  void shouldThrowIfHtmlIsNull() {
    assertThrows(NullPointerException.class, () -> extractLinks(null));
  }

  @Test
  void shouldNotProduceAnyLinksForEmptyHtml() {
    assertEquals(
        Collections.emptyList(),
        extractLinks("""
            <html>
              <head></head>
              <body></body>
            </html>
            """));
  }

  @Test
  void shouldIncludeLinksFromLinkTags() {
    assertEquals(
        List.of(
            "static/icons/favicon.ico",
            "static/css/awesome.css"),
        extractLinks("""
            <html>
              <head>
                <title>Whatever</title>
                <link rel="shortcut icon" href="static/icons/favicon.ico" />
                <link rel="stylesheet" type="text/css" href="static/css/awesome.css" />
              </head>
            </html>
            """));
  }

  @Test
  void shouldIgnoreLinkTagsWithoutHref() {
    assertEquals(
        List.of("static/theme.css"),
        extractLinks("""
            <html>
              <head>
                <title>Whatever</title>
                <!-- Oops, missing a href here! -->
                <link rel="shortcut icon" />
                <link rel="stylesheet" type="text/css" href="static/theme.css" />
              </head>
            </html>
            """));
  }

  @Test
  void shouldIncludeLinksFromATags() {
    assertEquals(
        List.of("user/1337/images"),
        extractLinks("""
            <html>
              <head>
                <title>Mr. X's images</title>
              </head>
              <body>
                <p>For Mr. X's images, click <a href="user/1337/images">here</a>!</p>
              </body>
            </html>
            """));
  }

  @Test
  void shouldIgnoreATagsWithoutHref() {
    assertEquals(
        Collections.emptyList(),
        extractLinks("""
            <html>
              <body>
                <p>What is this though: <a>link</a>?</p>
              </body>
            </html>
            """));
  }

  @Test
  void shouldProduceLinksInOrder() {
    /*
     * This is not really a requirement, but it is polite and least surprising to produce the links
     * in the order in which they appear in the HTML.
     */
    assertEquals(
        List.of("../my.css", "mailto:fres@tel.se", "/yours.css"),
        extractLinks("""
            <html>
              <head><link rel="stylesheet" href="../my.css" /></head>
              <body>
                <p>For interesting stuff, click <a href="mailto:fres@tel.se">here</a>?!</p>
              </body>
              <footer><link rel="stylesheet" href="/yours.css" /></footer>
            </html>
            """));
  }

  @Test
  void shouldIncludeImgTags() {
    assertEquals(
        List.of("http://cats.org/garfield.gif", "../local-cat.jpg"),
        extractLinks("""
            <html>
              <body>
                <img src="http://cats.org/garfield.gif" />
                <img /> <!-- Ouch, forgot src... -->
                <img src="../local-cat.jpg" />
              </body>
            </html>
            """));
  }

  @Test
  void shouldIncludeScriptTags() {
    assertEquals(
        List.of("http://some.cdn.org/coolstuff.min.js", "/site.cgi"),
        extractLinks("""
            <html>
              <body>
                <script src="http://some.cdn.org/coolstuff.min.js" />
                <script src="/site.cgi" />
              </body>
            </html>
            """));
  }

  @Test
  void shouldExtractLongdescFromImgTag() {
    assertEquals(
        List.of("http://anonymous.cats.com/about", "anonymous_cat.gif"),
        extractLinks("""
            <html>
              <img src="anonymous_cat.gif" longdesc="http://anonymous.cats.com/about">
            </html>
            """));
  }

}
