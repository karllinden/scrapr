package org.scrapr;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * A fake resource that can be served by a {@link FakeFetcher}.
 */
public interface FakeResource {

  HttpResponse<byte[]> toHttpResponse(HttpRequest request) throws InterruptedException, IOException;

  static FakeResource absent() {
    return request -> new SimpleHttpResponse(request, 404, null, new byte[0]);
  }

  static FakeFileResource file(String content) {
    return new FakeFileResource(content);
  }

  static FakeFileResource html(String html) {
    return file(html).withContentType("text/html");
  }

  static FakeResource interruptedException(InterruptedException exception) {
    return request -> {
      throw exception;
    };
  }

  static FakeResource ioException(IOException exception) {
    return request -> {
      throw exception;
    };
  }

  static FakeResource runtimeException(RuntimeException exception) {
    return request -> {
      throw exception;
    };
  }

}
