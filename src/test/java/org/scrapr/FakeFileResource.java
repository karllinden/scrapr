package org.scrapr;

import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * A fake file resource.
 */
public record FakeFileResource(String contentType, String content) implements FakeResource {

  public FakeFileResource {
    Objects.requireNonNull(content);
  }

  public FakeFileResource(String content) {
    this(null, content);
  }

  @Override
  public HttpResponse<byte[]> toHttpResponse(HttpRequest request) {
    byte[] bytes = content.getBytes(StandardCharsets.UTF_8);
    return new SimpleHttpResponse(request, 200, contentType, bytes);
  }

  public FakeFileResource withContentType(String contentType) {
    return new FakeFileResource(contentType, content);
  }

}
