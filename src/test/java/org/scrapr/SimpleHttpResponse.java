package org.scrapr;

import java.net.URI;
import java.net.http.HttpClient.Version;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.net.ssl.SSLSession;

/**
 * Simple implementation of {@link HttpResponse}.
 */
record SimpleHttpResponse(HttpRequest request, int statusCode, String contentType,
                          byte[] body) implements HttpResponse<byte[]> {

  public SimpleHttpResponse {
    Objects.requireNonNull(request);
    body = body.clone();
  }

  @Override
  public Optional<HttpResponse<byte[]>> previousResponse() {
    return Optional.empty();
  }

  @Override
  public HttpHeaders headers() {
    return HttpHeaders.of(getHeaderMap(), (k, v) -> true);
  }

  private Map<String, List<String>> getHeaderMap() {
    return contentType != null
        ? Map.of("Content-Type", List.of(contentType))
        : Collections.emptyMap();
  }

  @Override
  public byte[] body() {
    return body.clone();
  }

  @Override
  public Optional<SSLSession> sslSession() {
    return Optional.empty();
  }

  @Override
  public URI uri() {
    return request.uri();
  }

  @Override
  public Version version() {
    return Version.HTTP_1_1;
  }
}
