package org.scrapr.cli;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;
import org.scrapr.Saver;

/**
 * {@link Saver} decorator that adds printing the file that is currently being saved.
 */
final class PrintingSaver implements Saver {

  private final Saver saver;

  PrintingSaver(Saver saver) {
    this.saver = Objects.requireNonNull(saver);
  }

  @Override
  public void save(Path path, byte[] bytes) throws IOException {
    System.out.printf("Saving %s (%s bytes)...%n", path, bytes.length);
    saver.save(path, bytes);
  }
}
