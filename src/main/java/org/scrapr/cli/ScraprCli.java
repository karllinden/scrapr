package org.scrapr.cli;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.scrapr.FileSaver;
import org.scrapr.Scraper;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * Scraper command line interface.
 */
@Command(
    name = "scrapr",
    version = "1.0-SNAPSHOT",
    mixinStandardHelpOptions = true)
public final class ScraprCli implements Callable<Integer> {

  @Parameters(
      description = "The URI to start scraping at")
  private URI uri;

  @Option(
      names = {"-j", "--jobs"},
      description = "The number of fetch jobs to run in parallel",
      paramLabel = "<jobs>")
  private int jobs = 1;

  /**
   * Program entry point.
   *
   * @param args the arguments
   */
  public static void main(String[] args) {
    int exitCode = new CommandLine(new ScraprCli()).execute(args);
    System.exit(exitCode);
  }

  @Override
  public Integer call() throws InterruptedException, IOException {
    ExecutorService fetcherExecutor = Executors.newFixedThreadPool(jobs);
    try {
      scrapeWith(fetcherExecutor);
      return 0;
    } finally {
      fetcherExecutor.shutdownNow();
    }
  }

  private void scrapeWith(ExecutorService fetcherExecutor) throws IOException, InterruptedException {
    var scraper = new Scraper(uri);
    scraper.setFetcherExecutor(fetcherExecutor);

    // This is a simple, but kinda crude, way of printing progress.
    scraper.setSaver(new PrintingSaver(new FileSaver()));

    scraper.scrape();
  }

}
