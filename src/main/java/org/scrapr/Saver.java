package org.scrapr;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Interface for saving the results of the fetched pages.
 */
@FunctionalInterface
public interface Saver {

  /**
   * Saves the given bytes to the given path.
   *
   * @param path  the path to save the bytes to
   * @param bytes the bytes to save
   * @throws IOException if an I/O error occurs
   */
  void save(Path path, byte[] bytes) throws IOException;

}
