package org.scrapr;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Predicate;

/**
 * Performs the scraping.
 */
public final class Scraper {

  private final URI initialUri;

  // The paths that have already been visited by this Scraper instance. Used to avoid scraping the
  // same page twice.
  private final Set<String> visitedPaths = new HashSet<>();

  // The futures that we have recorded for the saving tasks that are running on the disk thread.
  // These are used to ensure that any exceptions thrown from the saver are thrown from scrape().
  private final Deque<Future<?>> saveFutures = new ArrayDeque<>();

  private ExecutorCompletionService<HttpResponse<byte[]>> fetcherService;

  private int nFetchesInProgress = 0;

  private Fetcher fetcher = createDefaultFetcher();

  private Saver saver = new FileSaver();

  /**
   * Creates a new scraper.
   *
   * @param uri the URI to start scraping at
   */
  public Scraper(URI uri) {
    initialUri = uri;
    checkInitialUri();
    setFetcherExecutor(Executors.newSingleThreadExecutor());
  }

  private void checkInitialUri() {
    if (initialUri.getScheme() == null) {
      throw new IllegalArgumentException(
          "The URI does not have a scheme (http or https): " + initialUri);
    } else if (initialUri.getHost() == null) {
      throw new IllegalArgumentException("The URI does not have host: " + initialUri);
    }
  }

  /**
   * Sets the fetcher to use.
   *
   * <p>
   * By default, the HTTP requests are made with a real HTTP client.
   * </p>
   *
   * @param fetcher the fetcher to use
   */
  public void setFetcher(Fetcher fetcher) {
    this.fetcher = Objects.requireNonNull(fetcher);
  }

  /**
   * Sets the saver to use.
   *
   * @param saver the saver
   */
  public void setSaver(Saver saver) {
    this.saver = Objects.requireNonNull(saver);
  }

  /**
   * Sets the executor service to use for fetching.
   *
   * <p>
   * By default a single threaded executor is used.
   * </p>
   *
   * @param fetcherExecutor the fetcher executor
   */
  public void setFetcherExecutor(ExecutorService fetcherExecutor) {
    this.fetcherService = new ExecutorCompletionService<>(fetcherExecutor);
  }

  /**
   * Scrapes the site starting at the URI given to the constructor.
   *
   * @throws IOException          if an I/O error occurs
   * @throws InterruptedException if interrupted
   */
  public void scrape() throws IOException, InterruptedException {
    // We expect the fetching latency to dominate, so writing to disk in more than one thread would
    // likely only starve the disk threads. The important thing is that  writes to the disk do not
    // block the downloads, because those are two completely different I/O sources.
    ExecutorService saverService = Executors.newSingleThreadExecutor();
    try {
      scrapeAndSaveConcurrently(saverService);
      handleRemainingSaves();
    } finally {
      saverService.shutdownNow();
    }
  }

  private void scrapeAndSaveConcurrently(ExecutorService saverService)
      throws InterruptedException, IOException {
    fetch(initialUri);

    HttpResponse<byte[]> response;
    while ((response = nextResponse()) != null) {
      fetchLinks(response);
      saveBody(saverService, response);
      handleDoneSaves();
    }
  }

  // Handles any save tasks that have completed. This is to prevent the saveFutures from growing
  // unnecessarily large, and to throw any exceptions from the task as early as possible.
  private void handleDoneSaves() throws IOException, InterruptedException {
    handleSaves(Future::isDone);
  }

  private void handleRemainingSaves() throws InterruptedException, IOException {
    handleSaves(future -> true);
  }

  private void handleSaves(Predicate<Future<?>> predicate)
      throws IOException, InterruptedException {
    while (!saveFutures.isEmpty() && predicate.test(saveFutures.peekFirst())) {
      getResult(saveFutures.removeFirst());
    }
  }

  private void saveBody(ExecutorService saverService, HttpResponse<byte[]> response) {
    Future<?> future = saverService.submit(() -> {
      saver.save(toSavePath(response), response.body());
      return null;
    });
    saveFutures.addLast(future);
  }

  private void fetchLinks(HttpResponse<byte[]> response) {
    if (!isHtml(response)) {
      // No links to fetch.
      return;
    }

    ScrapeUtils.extractLinks(new String(response.body())).stream()
        .map(response.uri()::resolve)
        .filter(this::isSameOrigin)
        .forEach(this::fetch);
  }

  private boolean isSameOrigin(URI uri) {
    return initialUri.getHost().equals(uri.getHost())
        && initialUri.getScheme().equals(uri.getScheme());
  }

  private static boolean isHtml(HttpResponse<?> response) {
    return response.headers().firstValue("Content-Type")
        .filter(ct -> ct.equals("text/html"))
        .isPresent();
  }

  private Path toSavePath(HttpResponse<byte[]> response) {
    String pathWithoutSlash = response.uri().getPath().substring(1);
    return Path.of(initialUri.getHost()).resolve(pathWithoutSlash);
  }

  private void fetch(URI uri) {
    URI withIndex = withDirectoryIndex(uri).normalize();
    if (!visitedPaths.add(withIndex.getPath())) {
      return;
    }

    HttpRequest request = HttpRequest.newBuilder()
        .uri(withIndex)
        .build();
    fetcherService.submit(() -> fetcher.send(request));
    nFetchesInProgress++;
  }

  private HttpResponse<byte[]> nextResponse() throws InterruptedException, IOException {
    if (nFetchesInProgress == 0) {
      return null;
    }

    Future<HttpResponse<byte[]>> future = fetcherService.take();
    nFetchesInProgress--;
    return getResult(future);
  }

  private static <T> T getResult(Future<T> future) throws InterruptedException, IOException {
    try {
      return future.get();
    } catch (ExecutionException ex) {
      Throwable cause = ex.getCause();
      if (cause instanceof IOException ioException) {
        throw ioException;
      } else if (cause instanceof InterruptedException interruptedException) {
        throw interruptedException;
      } else if (cause instanceof RuntimeException runtimeException) {
        throw runtimeException;
      } else {
        throw new IllegalStateException("The task is not allowed to throw this exception.", cause);
      }
    }
  }

  /*
   * It is a bit ugly that we assume all directory indices are index.html, but it is good enough
   * now. A real scraper would use the Content-Type together with some heuristics to correctly
   * handle index.php, index.asp, ...
   */
  private static URI withDirectoryIndex(URI uri) {
    String path = uri.getPath();
    if (path == null || path.equals("")) {
      return uri.resolve("/index.html");
    } else if (path.endsWith("/")) {
      return uri.resolve("index.html");
    } else {
      return uri;
    }
  }

  private static Fetcher createDefaultFetcher() {
    HttpClient client = HttpClient.newHttpClient();
    return request -> client.send(request, BodyHandlers.ofByteArray());
  }

}
