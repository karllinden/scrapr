package org.scrapr;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Stream;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Static utility methods needed for scraping.
 */
public final class ScrapeUtils {

  private static final List<String> LINK_ATTRIBUTE_KEYS = List.of("href", "longdesc", "src");

  private ScrapeUtils() {
    //
  }

  /**
   * Extracts all links from the given HTML.
   *
   * <p>
   * This method currently handles the simple cases with {@code href} and {@code src}. It can be
   * modified to include more links from
   * <a href="https://www.w3.org/TR/REC-html40/index/attributes.html">this table</a> if that is
   * necessary in the future.
   * </p>
   *
   * @param html the HTML
   * @return the links
   */
  public static List<String> extractLinks(String html) {
    Document document = Jsoup.parse(html);
    return document.getAllElements().stream()
        .flatMap(ScrapeUtils::extractLinks)
        .toList();
  }

  private static Stream<String> extractLinks(Element element) {
    return LINK_ATTRIBUTE_KEYS.stream()
        .filter(element::hasAttr)
        .map(element::attr);
  }

}
