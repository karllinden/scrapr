package org.scrapr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Saves the fetched bytes to a file.
 *
 * <p>
 * This is the default implementation.
 * </p>
 */
public final class FileSaver implements Saver {

  @Override
  public void save(Path path, byte[] bytes) throws IOException {
    Files.createDirectories(path.getParent());
    Files.write(path, bytes);
  }

}
