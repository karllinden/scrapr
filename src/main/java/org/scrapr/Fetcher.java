package org.scrapr;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Interface for fetching HTTP resources using an {@link HttpRequest}.
 */
@FunctionalInterface
public interface Fetcher {

  /**
   * Sends an HTTP request.
   *
   * @param request the request
   * @return the response
   * @throws InterruptedException if interrupted during the request
   * @throws IOException if an I/O error occurs
   */
  HttpResponse<byte[]> send(HttpRequest request) throws InterruptedException, IOException;

}
